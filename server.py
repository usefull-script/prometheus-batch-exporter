from flask import Flask, Response
app = Flask(__name__)

@app.route("/probe")
def metrics():
    try:
        with open ('PATH/TO/metric', 'r') as file:
            data = file.read()
        return Response(data, mimetype='text/plain')
    except Exception as e:
        return Response(f"Error reading file: {str(e)}", status=500, mimetype='text/plain')

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=9118)