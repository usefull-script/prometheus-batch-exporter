# Prometheus Batch Exporter

## Prerequisite

- Python 3
- Flask


## File Modifications

**script.sh** should be added inside the batches you want to monitor. You should change "batchName" by the name of your batch. You can also add other labels inside the brackets
```console
"batch_status{batch=\"batchName\"}
```
You can also change the path to the file named "metric"

In **server.py** change the path to the metric file created in the script
```python
try:
        with open ('PATH/TO/metric', 'r') as file:
```
In the same file, modify your port to an unused one
```python
app.run(host="0.0.0.0", port=PORT)
```

In **/prometheus/prometheus.yml** change add your IP address and the port chosen above
```YAML
static_configs:
      - targets: ["MY_IP:PORT"]
```

To run your server as a service create the file **/etc/systemd/system/flask.service** and change the ExecStart with the path to your **server.py**
```
ExecStart=/usr/bin/python3 /PATH/TO/server.py
```

## Deploy

Reload systemd manager configuration
```console
systemctl daemon-reload
```

Start your new service if it didn't
```console
systemctl start flask.service
```

Make sure your batch is running with a cron
```console
crontab -e
```

If your Prometheus run inside a container restart it after modifying your **prometheus.yml** through a volume 
```console
docker restart <container>
```